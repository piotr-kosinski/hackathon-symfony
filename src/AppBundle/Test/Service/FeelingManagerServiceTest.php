<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-04
 * Time: 05:11
 */

namespace AppBundle\Test;

use AppBundle\Document\Feeling;
use AppBundle\Exception\DatabaseException;
use AppBundle\Exception\SerializerException;
use AppBundle\Service\FeelingManagerService;
use Doctrine\ODM\MongoDB\DocumentManager;
use JMS\Serializer\Serializer;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class FeelingManagerServiceTest
 * @package AppBundle\Test
 */
class FeelingManagerServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var  DocumentManager | \PHPUnit_Framework_MockObject_MockObject */
    private $documentManager;

    /** @var  Serializer | \PHPUnit_Framework_MockObject_MockObject */
    private $serializer;

    /** @var  Logger | \PHPUnit_Framework_MockObject_MockObject */
    private $logger;

    /** @var  FeelingManagerService | \PHPUnit_Framework_MockObject_MockObject */
    private $service;

    public function setUp()
    {
        $this->documentManager = $this->getMockBuilder(DocumentManager::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'persist',
                    'flush'
                ]
            )
            ->getMock();

        $this->serializer = $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(['fromArray'])
            ->getMock();

        $this->logger = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->setMethods(['log'])
            ->getMock();

        $this->service = $this->getMockBuilder(FeelingManagerService::class)
            ->setConstructorArgs(
                [
                    $this->documentManager,
                    $this->serializer,
                    $this->logger
                ]
            )
            ->setMethods(null)
            ->getMock();
    }

    public function testDeserializeValid()
    {
        $feelingEntity = new Feeling();

        $this->serializer->expects(static::once())
            ->method('fromArray')
            ->willReturn(new $feelingEntity);

        $result = $this->service->deserialize(['json' => 'array']);
        static::assertEquals($feelingEntity, $result);
    }

    public function testDeserializeInvalid()
    {
        $this->serializer->expects(static::once())
            ->method('fromArray')
            ->willThrowException(new \Exception());

        $this->setExpectedException(SerializerException::class);

        $this->service->deserialize(['json' => 'array']);
    }


    public function testSaveFeelingValid()
    {
        $feelingEntity = new Feeling();

        $this->documentManager->expects(static::once())
            ->method('persist');

        $this->documentManager->expects(static::once())
            ->method('flush');

        $this->service->saveFeeling($feelingEntity);
    }

    public function testSaveFeelingInvalid()
    {
        $this->documentManager->expects(static::once())
            ->method('flush')
            ->willThrowException(new \Exception());

        $this->setExpectedException(DatabaseException::class);

        $this->service->saveFeeling(new Feeling());
    }
}