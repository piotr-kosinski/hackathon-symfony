<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-04
 * Time: 04:34
 */

namespace AppBundle\Exception;

/**
 * Class DatabaseException
 * @package AppBundle\Exception
 */
class DatabaseException extends \Exception
{

}
