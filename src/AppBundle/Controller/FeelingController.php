<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-03
 * Time: 17:26
 */

namespace AppBundle\Controller;

use AppBundle\Exception\SerializerException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FeelingController
 * @package AppBundle\Controller
 */
class FeelingController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postFeelingsAction(Request $request)
    {
        $header = ['Access-Control-Allow-Origin' => '*'];

        try {
            $manager = $this->container->get('app.feeling_manager');
            $postData = $request->request->all();
            $postJson = $manager->parseRequestData($postData);
            $feelingEntity = $manager->deserialize($postJson);
            $manager->saveFeeling($feelingEntity);
        } catch (SerializerException $e) {
            return new JsonResponse(
                [
                    'success' => false,
                    'message' => $e->getMessage()
                ],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false], Response::HTTP_INTERNAL_SERVER_ERROR, $header);
        }

        return new JsonResponse(
            [
                'success' => true,
                'id' => $feelingEntity->getId()
            ],
            Response::HTTP_OK,
            $header
        );
    }
}
