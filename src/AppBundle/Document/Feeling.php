<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-03
 * Time: 21:25
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ODM\Document(
 *     collection="feeling",
 *     repositoryClass="AppBundle\Repository\FeelingRepository"
 * )
 */
class Feeling
{
    /**
     * @var string
     * @ODM\Id(strategy="auto")
     * @JMS\Exclude
     */
    protected $id;

    /**
     * @var Emotion
     * @ODM\EmbedOne(targetDocument="AppBundle\Document\Emotion")
     * @JMS\Type("AppBundle\Document\Emotion")
     * @JMS\SerializedName("emotions")
     */
    private $emotions;

    /**
     * @var Expression
     * @ODM\EmbedOne(targetDocument="AppBundle\Document\Expression")
     * @JMS\Type("AppBundle\Document\Expression")
     * @JMS\SerializedName("expressions")
     */
    private $expressions;

    /**
     * @var \DateTime
     * @ODM\Date()
     * @JMS\Exclude
     */
    private $created;

    /**
     * @var string
     * @ODM\String()
     * @JMS\Type("string")
     * @JMS\SerializedName("elements")
     */
    private $elements;

    /**
     * Feeling constructor.
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return Emotion
     */
    public function getEmotions()
    {
        return $this->emotions;
    }

    /**
     * @param Emotion $emotions
     *
     * @return $this
     */
    public function setEmotions($emotions)
    {
        $this->emotions = $emotions;

        return $this;
    }

    /**
     * @return Expression
     */
    public function getExpressions()
    {
        return $this->expressions;
    }

    /**
     * @param Expression $expressions
     *
     * @return $this
     */
    public function setExpressions($expressions)
    {
        $this->expressions = $expressions;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }


    /**
     * @return string
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param string $elements
     *
     * @return $this
     */
    public function setElements($elements)
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
