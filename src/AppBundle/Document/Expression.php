<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-03
 * Time: 22:06
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;

/** @ODM\EmbeddedDocument */
class Expression
{
    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $smile;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("innerBrowRaise")
     */
    private $innerBrowRaise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("browRaise")
     */
    private $browRaise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("browFurrow")
     */
    private $browFurrow;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("noseWrinkle")
     */
    private $noseWrinkle;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("upperLipRaise")
     */
    private $upperLipRaise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lipCornerDepressor")
     */
    private $lipCornerDepressor;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("chinRaise")
     */
    private $chinRaise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lipPucker")
     */
    private $lipPucker;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lipPress")
     */
    private $lipPress;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lipSuck")
     */
    private $lipSuck;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("mouthOpen")
     */
    private $mouthOpen;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("smirk")
     */
    private $smirk;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("eyeClosure")
     */
    private $eyeClosure;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("attention")
     */
    private $attention;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lidTighten")
     */
    private $lidTighten;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("jawDrop")
     */
    private $jawDrop;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("dimpler")
     */
    private $dimpler;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("eyeWiden")
     */
    private $eyeWiden;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("cheekRaise")
     */
    private $cheekRaise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     * @JMS\SerializedName("lipStretch")
     */
    private $lipStretch;

    /**
     * @return float
     */
    public function getSmile()
    {
        return $this->smile;
    }

    /**
     * @param float $smile
     *
     * @return $this
     */
    public function setSmile($smile)
    {
        $this->smile = $smile;

        return $this;
    }

    /**
     * @return float
     */
    public function getInnerBrowRaise()
    {
        return $this->innerBrowRaise;
    }

    /**
     * @param float $innerBrowRaise
     *
     * @return $this
     */
    public function setInnerBrowRaise($innerBrowRaise)
    {
        $this->innerBrowRaise = $innerBrowRaise;

        return $this;
    }

    /**
     * @return float
     */
    public function getBrowRaise()
    {
        return $this->browRaise;
    }

    /**
     * @param float $browRaise
     *
     * @return $this
     */
    public function setBrowRaise($browRaise)
    {
        $this->browRaise = $browRaise;

        return $this;
    }

    /**
     * @return float
     */
    public function getBrowFurrow()
    {
        return $this->browFurrow;
    }

    /**
     * @param float $browFurrow
     *
     * @return $this
     */
    public function setBrowFurrow($browFurrow)
    {
        $this->browFurrow = $browFurrow;

        return $this;
    }

    /**
     * @return float
     */
    public function getNoseWrinkle()
    {
        return $this->noseWrinkle;
    }

    /**
     * @param float $noseWrinkle
     *
     * @return $this
     */
    public function setNoseWrinkle($noseWrinkle)
    {
        $this->noseWrinkle = $noseWrinkle;

        return $this;
    }

    /**
     * @return float
     */
    public function getUpperLipRaise()
    {
        return $this->upperLipRaise;
    }

    /**
     * @param float $upperLipRaise
     *
     * @return $this
     */
    public function setUpperLipRaise($upperLipRaise)
    {
        $this->upperLipRaise = $upperLipRaise;

        return $this;
    }

    /**
     * @return float
     */
    public function getLipCornerDepressor()
    {
        return $this->lipCornerDepressor;
    }

    /**
     * @param float $lipCornerDepressor
     *
     * @return $this
     */
    public function setLipCornerDepressor($lipCornerDepressor)
    {
        $this->lipCornerDepressor = $lipCornerDepressor;

        return $this;
    }

    /**
     * @return float
     */
    public function getChinRaise()
    {
        return $this->chinRaise;
    }

    /**
     * @param float $chinRaise
     *
     * @return $this
     */
    public function setChinRaise($chinRaise)
    {
        $this->chinRaise = $chinRaise;

        return $this;
    }

    /**
     * @return float
     */
    public function getLipPucker()
    {
        return $this->lipPucker;
    }

    /**
     * @param float $lipPucker
     *
     * @return $this
     */
    public function setLipPucker($lipPucker)
    {
        $this->lipPucker = $lipPucker;

        return $this;
    }

    /**
     * @return float
     */
    public function getLipPress()
    {
        return $this->lipPress;
    }

    /**
     * @param float $lipPress
     *
     * @return $this
     */
    public function setLipPress($lipPress)
    {
        $this->lipPress = $lipPress;

        return $this;
    }

    /**
     * @return float
     */
    public function getLipSuck()
    {
        return $this->lipSuck;
    }

    /**
     * @param float $lipSuck
     *
     * @return $this
     */
    public function setLipSuck($lipSuck)
    {
        $this->lipSuck = $lipSuck;

        return $this;
    }

    /**
     * @return float
     */
    public function getMouthOpen()
    {
        return $this->mouthOpen;
    }

    /**
     * @param float $mouthOpen
     *
     * @return $this
     */
    public function setMouthOpen($mouthOpen)
    {
        $this->mouthOpen = $mouthOpen;

        return $this;
    }

    /**
     * @return float
     */
    public function getSmirk()
    {
        return $this->smirk;
    }

    /**
     * @param float $smirk
     *
     * @return $this
     */
    public function setSmirk($smirk)
    {
        $this->smirk = $smirk;

        return $this;
    }

    /**
     * @return float
     */
    public function getEyeClosure()
    {
        return $this->eyeClosure;
    }

    /**
     * @param float $eyeClosure
     *
     * @return $this
     */
    public function setEyeClosure($eyeClosure)
    {
        $this->eyeClosure = $eyeClosure;

        return $this;
    }

    /**
     * @return float
     */
    public function getAttention()
    {
        return $this->attention;
    }

    /**
     * @param float $attention
     *
     * @return $this
     */
    public function setAttention($attention)
    {
        $this->attention = $attention;

        return $this;
    }

    /**
     * @return float
     */
    public function getLidTighten()
    {
        return $this->lidTighten;
    }

    /**
     * @param float $lidTighten
     *
     * @return $this
     */
    public function setLidTighten($lidTighten)
    {
        $this->lidTighten = $lidTighten;

        return $this;
    }

    /**
     * @return float
     */
    public function getJawDrop()
    {
        return $this->jawDrop;
    }

    /**
     * @param float $jawDrop
     *
     * @return $this
     */
    public function setJawDrop($jawDrop)
    {
        $this->jawDrop = $jawDrop;

        return $this;
    }

    /**
     * @return float
     */
    public function getDimpler()
    {
        return $this->dimpler;
    }

    /**
     * @param float $dimpler
     *
     * @return $this
     */
    public function setDimpler($dimpler)
    {
        $this->dimpler = $dimpler;

        return $this;
    }

    /**
     * @return float
     */
    public function getEyeWiden()
    {
        return $this->eyeWiden;
    }

    /**
     * @param float $eyeWiden
     *
     * @return $this
     */
    public function setEyeWiden($eyeWiden)
    {
        $this->eyeWiden = $eyeWiden;

        return $this;
    }

    /**
     * @return float
     */
    public function getCheekRaise()
    {
        return $this->cheekRaise;
    }

    /**
     * @param float $cheekRaise
     *
     * @return $this
     */
    public function setCheekRaise($cheekRaise)
    {
        $this->cheekRaise = $cheekRaise;

        return $this;
    }

    /**
     * @return float
     */
    public function getLipStretch()
    {
        return $this->lipStretch;
    }

    /**
     * @param float $lipStretch
     *
     * @return $this
     */
    public function setLipStretch($lipStretch)
    {
        $this->lipStretch = $lipStretch;

        return $this;
    }
}