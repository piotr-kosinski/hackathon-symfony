<?php
/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-03
 * Time: 22:04
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;

/** @ODM\EmbeddedDocument */
class Emotion
{
    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $joy;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $sadness;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $disgust;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $contempt;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $anger;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $fear;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $surprise;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $valence;

    /**
     * @var float
     * @ODM\Float()
     * @JMS\Type("double")
     */
    private $engagement;

    /**
     * @return float
     */
    public function getJoy()
    {
        return $this->joy;
    }

    /**
     * @param float $joy
     *
     * @return $this
     */
    public function setJoy($joy)
    {
        $this->joy = $joy;

        return $this;
    }

    /**
     * @return float
     */
    public function getSadness()
    {
        return $this->sadness;
    }

    /**
     * @param float $sadness
     *
     * @return $this
     */
    public function setSadness($sadness)
    {
        $this->sadness = $sadness;

        return $this;
    }

    /**
     * @return float
     */
    public function getDisgust()
    {
        return $this->disgust;
    }

    /**
     * @param float $disgust
     *
     * @return $this
     */
    public function setDisgust($disgust)
    {
        $this->disgust = $disgust;

        return $this;
    }

    /**
     * @return float
     */
    public function getContempt()
    {
        return $this->contempt;
    }

    /**
     * @param float $contempt
     *
     * @return $this
     */
    public function setContempt($contempt)
    {
        $this->contempt = $contempt;

        return $this;
    }

    /**
     * @return float
     */
    public function getAnger()
    {
        return $this->anger;
    }

    /**
     * @param float $anger
     *
     * @return $this
     */
    public function setAnger($anger)
    {
        $this->anger = $anger;

        return $this;
    }

    /**
     * @return float
     */
    public function getFear()
    {
        return $this->fear;
    }

    /**
     * @param float $fear
     *
     * @return $this
     */
    public function setFear($fear)
    {
        $this->fear = $fear;

        return $this;
    }

    /**
     * @return float
     */
    public function getSurprise()
    {
        return $this->surprise;
    }

    /**
     * @param float $surprise
     *
     * @return $this
     */
    public function setSurprise($surprise)
    {
        $this->surprise = $surprise;

        return $this;
    }

    /**
     * @return float
     */
    public function getValence()
    {
        return $this->valence;
    }

    /**
     * @param float $valence
     *
     * @return $this
     */
    public function setValence($valence)
    {
        $this->valence = $valence;

        return $this;
    }

    /**
     * @return float
     */
    public function getEngagement()
    {
        return $this->engagement;
    }

    /**
     * @param float $engagement
     *
     * @return $this
     */
    public function setEngagement($engagement)
    {
        $this->engagement = $engagement;

        return $this;
    }
}
