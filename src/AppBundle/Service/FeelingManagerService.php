<?php

/**
 * Created by PhpStorm.
 * User: jacek.kubicki
 * Date: 2016-09-04
 * Time: 04:24
 */

namespace AppBundle\Service;

use AppBundle\Document\Feeling;
use AppBundle\Exception\DatabaseException;
use AppBundle\Exception\SerializerException;
use Doctrine\ODM\MongoDB\DocumentManager;
use JMS\Serializer\Serializer;
use Symfony\Bridge\Monolog\Logger;

/**
 * Class FeelingManagerService
 * @package AppBundle\Service
 */
class FeelingManagerService
{
    /** @var  DocumentManager */
    protected $documentManager;

    /** @var  Serializer */
    protected $serializer;

    /** @var  Logger */
    protected $logger;

    /**
     * FeelingManagerService constructor.
     *
     * @param DocumentManager $documentManager
     * @param Serializer      $serializer
     * @param Logger          $logger
     */
    public function __construct(DocumentManager $documentManager, Serializer $serializer, Logger $logger)
    {
        $this->documentManager = $documentManager;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * @param array $json
     *
     * @return Feeling
     *
     * @throws SerializerException
     */
    public function deserialize($json)
    {
        try {
            /** @var Feeling $feelingEntity */
            $feelingEntity = $this->serializer->fromArray(
                $json,
                Feeling::class
            );
        } catch (\Exception $e) {
            $this->logger->log(
                Logger::ALERT, 'deserialization failed: ' . $e . '. Argument json:' . json_encode($json)
            );
            throw new SerializerException($e);
        }

        return $feelingEntity;
    }

    /**
     * @param Feeling $feeling
     *
     * @throws DatabaseException
     */
    public function saveFeeling(Feeling $feeling)
    {
        try {
            $this->documentManager->persist($feeling);
            $this->documentManager->flush();
        } catch (\Exception $e) {
            $this->logger->log(Logger::ALERT, 'unable to save Feeling: ' . $e);
            throw new DatabaseException($e);
        }
    }

    /**
     * @param array $postData
     *
     * @return array
     */
    public function parseRequestData(array $postData)
    {
        $resultJson = [];

        if (isset($postData['data']['emotions'])) {
            $resultJson['emotions'] = json_decode($postData['data']['emotions'], true);
        }

        if (isset($postData['data']['expressions'])) {
            $resultJson['expressions'] = json_decode($postData['data']['expressions'], true);
        }

        if (array_key_exists('elements', $postData)) {
            $resultJson['elements'] = $postData['elements'];
        }

        return $resultJson;
    }
}
